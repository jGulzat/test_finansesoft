//
//  Navigation.swift
//  MetCastApp
//
//  Created by Gulzat on 19/6/24.
//

import SwiftUI

struct RoomInfo: Identifiable, Hashable {
    var id = UUID()
    let name: String
    let image: Image
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(name)
    }
}

struct RoomDetail: View {
    
    let roomInfo: RoomInfo
  
    var body: some View {
        VStack {
            roomInfo.image
                .font(.system(size: 56))
                .foregroundColor(.accentColor)
            Text(roomInfo.name)
                .font(.system(size: 24))
        }
        .padding()
    }
}

struct RoomsListView: View {
    
    private var rooms: [RoomInfo]
    
    init(rooms: [RoomInfo]) {
        self.rooms = rooms
    }
    
    var body: some View {
        NavigationStack {
            List(rooms) { room in
                NavigationLink(room.name, value: room)
            }
            .navigationTitle("House Rooms")
            .navigationDestination(for: RoomInfo.self) { room in
                RoomDetail(roomInfo: room)
            }
        }
    }
}
