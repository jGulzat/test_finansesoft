//
//  MetCastAppApp.swift
//  MetCastApp
//
//  Created by Gulzat on 19/6/24.
//

import SwiftUI

@main
struct MetCastAppApp: App {
   
    var body: some Scene {
        WindowGroup {
            SearchCityScreen()
        }
    }
}
