//
//  Network.swift
//  MetCastApp
//
//  Created by Gulzat on 19/6/24.
//

import Foundation
import SwiftUI

struct Network {
    
    let apiKey = "63c619d507d24cd2d6b724cb151868c3"
    let baseUrlString = "https://api.openweathermap.org/data/2.5/"
    static let shared = Network()
    
    private init () { }
    
    private func buildURL(queryItems: [URLQueryItem], endPoint: String) -> URL? {
        
        var urlComponents = URLComponents(string: baseUrlString + endPoint)
        
        var baseQueryItems = [
            URLQueryItem(name: "appid", value: apiKey),
            URLQueryItem(name: "units", value: "metric")
        ]
        
        baseQueryItems.append(contentsOf: queryItems)
        urlComponents?.queryItems = baseQueryItems
        return urlComponents?.url
    }
    
    func searchCities(query: String, completion: @escaping ([CityInfo]?, Error?) -> Void) {
        let queryEncoded = query.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
       
        let queryItems = [URLQueryItem(name: "q", value: queryEncoded)]
        guard let url = buildURL(queryItems: queryItems, endPoint: "find") else {
            completion(nil, NSError(domain: "Invalid URL", code: 0, userInfo: nil))
            return
        }
        
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                completion(nil, error)
                return
            }
            
            guard let data = data else {
                completion(nil, NSError(domain: "No data returned", code: 0, userInfo: nil))
                return
            }
            
            do {
                let decoder = JSONDecoder()
                let response = try decoder.decode(CitiesInfo.self, from: data)
                completion(response.list, nil)
            } catch let parseError {
                completion(nil, parseError)
            }
        }
        
        task.resume()
    }
    
    func getCityForecast(latitude: Double, longitude: Double, completion: @escaping (Array<[CityWeather]>?) -> Void) {
        let queryItems = [
            URLQueryItem(name: "lat", value: "\(latitude)"),
            URLQueryItem(name: "lon", value: "\(longitude)")
        ]
        
        guard let url = buildURL(queryItems: queryItems, endPoint: "forecast") else {
            completion(nil)
            return
        }
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            if let data = data {
                do {
                    let decoder = JSONDecoder()
                    let weatherResponse = try decoder.decode(CityForecast.self, from: data)
                    var groupedWeather = Array(Dictionary.init(grouping: weatherResponse.list) {$0.dtTxt.eraseDate}.values)
                    groupedWeather.sort{ $0.first?.dtTxt ?? "" < $1.first?.dtTxt ?? ""}
                    completion(groupedWeather)
                } catch {
                    print("Error decoding JSON: \(error)")
                    completion(nil)
                }
            } else {
                completion(nil)
            }
        }
        .resume()
    }
}
