//
//  BaseViewModel.swift
//  MetCastApp
//
//  Created by Gulzat on 20/6/24.
//

import Foundation

class BaseViewModel: ObservableObject {
    
    @Published var isLoad: Bool = false
}
