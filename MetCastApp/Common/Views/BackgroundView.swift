//
//  BackgroundView.swift
//  MetCastApp
//
//  Created by Gulzat on 19/6/24.
//

import SwiftUI

struct BackgroundView: View {
    
    @Binding var isLoad: Bool
    
    var body: some View {
        ZStack {
            LinearGradient(gradient: Gradient(colors: [.blue, .purple]), startPoint: .topLeading, endPoint: .bottomTrailing)
                .edgesIgnoringSafeArea(.all)
            
            if isLoad {
                ProgressView()
            }
        }
    }
}

struct CustomStyle: ViewModifier {
    func body(content: Content) -> some View {
        content
            .padding(.all, 8)
            .cornerRadius(20)
            .overlay(
                RoundedRectangle(cornerRadius: 20)
                    .stroke(Color.white.opacity(0.8), lineWidth: 1)
            )
    }
}

extension View {
    func customStyle() -> some View {
        modifier(CustomStyle())
    }
}
