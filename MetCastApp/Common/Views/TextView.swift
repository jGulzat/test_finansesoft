//
//  Text.swift
//  MetCastApp
//
//  Created by Gulzat on 20/6/24.
//

import SwiftUI

struct TextView: View {
    let title: String
    let size: CGFloat
    let weight: Font.Weight
    let foregroundColor: Color
    
    init(title: String, size: CGFloat = 12, weight: Font.Weight = .heavy, foregroundColor: Color = .white) {
        self.title = title
        self.size = size
        self.weight = weight
        self.foregroundColor = foregroundColor
    }
    
    var body: some View {
        Text(title)
            .font(.system(size: size, weight: weight))
            .foregroundColor(foregroundColor)
    }
}
