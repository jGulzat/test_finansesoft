//
//  SearchCityViewModel.swift
//  MetCastApp
//
//  Created by Gulzat on 20/6/24.
//

import SwiftUI

final class SearchCityViewModel: BaseViewModel {
    
    @Published var cityName: String = "" {
        didSet {
            searchError = ""
        }
    }
    @Published var searchError: String = ""
    @Published var selectedCity: CityInfo?
    @Published var listOfCity: [CityInfo] = []
    
    func searchCities() {
        isLoad = true
        Network.shared.searchCities(query: cityName) { listOfCity, error in
            DispatchQueue.main.async {
                self.isLoad = false
                guard let cities = listOfCity else {
                    self.searchError = "Попробуйте другое название"
                    return
                }
                self.listOfCity = cities
                self.searchError = ""
            }
        }
    }
    
    func searchPlaceHolder() -> String {
        searchError.isEmpty ? "Название города" : searchError
    }
    
    func validateEnteredCity() {
        if cityName.isEmpty {
            searchError = "Введите название города!"
        } else {
            searchCities()
        }
    }
    
}
