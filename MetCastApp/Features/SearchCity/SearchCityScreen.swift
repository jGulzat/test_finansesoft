//
//  ContentView.swift
//  MetCastApp
//
//  Created by Gulzat on 19/6/24.
//

import SwiftUI

struct SearchCityScreen: View {
    
    @ObservedObject private var viewModel = SearchCityViewModel()
    
    var body: some View {
        NavigationView {
            ZStack {
                BackgroundView(isLoad: $viewModel.isLoad)
                
                VStack(alignment: .leading) {
                    TextView(
                        title: "Введите название вашего города и выберите из найденных вариантов",
                        size: 16, 
                        weight: .regular
                    )
                   
                    HStack(alignment: .lastTextBaseline) {
                        VStack(alignment: .leading, spacing: .zero) {
                            Text(viewModel.searchError)
                                .foregroundColor(viewModel.searchError.isEmpty ? .clear : .red)
                            
                            TextField(
                                viewModel.searchPlaceHolder(),
                                text: $viewModel.cityName
                            )
                            .foregroundColor(Color.white.opacity(0.9))
                            .accentColor(Color.white.opacity(0.9))
                        }
                        
                        Button(action: {
                            hideKeyboard()
                            viewModel.searchCities()
                        }, label: {
                            TextView(title: "Найти", size: 16, foregroundColor: Color.white.opacity(0.9))
                                .customStyle()
                        })
                    }
                    .padding(.top, 12)
                    
                    VStack(alignment: .leading) {
                        ForEach(Array(viewModel.listOfCity.enumerated()), id: \.offset) { index, city in
                            NavigationLink(destination: CityForecastScreen(city)) {
                                VStack(alignment: .leading) {
                                    
                                    TextView(title: "\(city.name) (\(city.sys.country))", size: 16)
                                        .frame(width: UIScreen.main.bounds.width - 100, alignment: .leading)
                                        
                                    TextView(title: "Координаты: \(city.coord.lat); \(city.coord.lon)")
                                }
                                .customStyle()
                            }
                        }
                    }
                    
                    Spacer()
                    
                }
                .padding(.all, 20)
            }
        }
    }
}



