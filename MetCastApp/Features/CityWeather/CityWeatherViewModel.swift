//
//  CityWeatherViewModel.swift
//  MetCastApp
//
//  Created by Gulzat on 19/6/24.
//

import SwiftUI

class CityForecastViewModel: BaseViewModel {
    
    let dateFormatter = DateFormatter()
    let calendar = Calendar.current
    let selectedCity: CityInfo
    
    @Published var selectedRangeWeather: CityWeather?
    @Published var selectedDayIndex: Int? {
        didSet {
            getWeatherByTime()
            selectedTimeIndex = 3
        }
    }
    @Published var weatherByTime: [CityWeather] = []
    @Published var selectedTimeIndex: Int = 3 {
        didSet {
            selectedRangeWeather = weatherByTime[selectedTimeIndex]
        }
    }
    @Published var cityForecast: Array<[CityWeather]> = Array()
    
    init(selectedCity: CityInfo) {
        self.selectedCity = selectedCity
    }
    
    func getCityForecast() {
        isLoad = true
        Network.shared.getCityForecast(latitude: selectedCity.coord.lat, longitude: selectedCity.coord.lon) { cityForecast in
            
            guard let forecast = cityForecast else { return }
            DispatchQueue.main.async {
                self.isLoad = false
                self.cityForecast = forecast
                self.selectedDayIndex = 0
                self.getWeatherByTime()
                self.selectedTimeIndex = 3
            }
        }
    }
    
    func getWeatherByTime() {
        guard let selectedDayIndex = selectedDayIndex else { return }
        let currentDayTimeWeather = cityForecast[selectedDayIndex]
        weatherByTime = currentDayTimeWeather
        if currentDayTimeWeather.count < 8 {
            let less = 8 - currentDayTimeWeather.count
            
            if cityForecast.count > selectedDayIndex + 1 {
                let nextDayWeather = cityForecast[selectedDayIndex + 1].prefix(less)
                weatherByTime.append(contentsOf: nextDayWeather)
            }
        }
    }
    
    func getDate(_ date: String) -> String {
        dateFormatter.dateFormat = "yyyy-MM-dd"
        if let date = dateFormatter.date(from: date) {
            let dateString = dateFormatter.string(from: date)
            return dateString
        }
        
        return ""
    }
}
