//
//  CityWeatherScreen.swift
//  MetCastApp
//
//  Created by Gulzat on 19/6/24.
//

import SwiftUI

struct CityForecastScreen: View {
    
    @ObservedObject private var viewModel: CityForecastViewModel
    
    init(_ selectedCity: CityInfo) {
        self.viewModel = CityForecastViewModel(selectedCity: selectedCity)
    }
    
    var body: some View {
        ZStack {
            BackgroundView(isLoad: $viewModel.isLoad)
                .ignoresSafeArea()
            
            VStack(alignment: .center) {
                TextView(title: "Weather", size: 40, weight: .heavy)
                
                TextView(title: viewModel.selectedCity.name, size: 28, weight: .regular)
                
                CityForecastContentView(viewModel: viewModel)
                if let weather = viewModel.selectedRangeWeather {
                    WeatherDetailView(weatherDetail: weather)
                        .padding(.top, 20)
                }
                
                Spacer()
                CityForecastBottomView(viewModel: viewModel)
                Spacer()
            }
            .padding(.all, 12)
        }
        .onAppear{
            viewModel.getCityForecast()
        }
    }
}

