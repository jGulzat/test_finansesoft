//
//  CityForecastContentView.swift
//  MetCastApp
//
//  Created by Gulzat on 20/6/24.
//

import SwiftUI

struct CityForecastContentView: View {
    
    @ObservedObject var viewModel: CityForecastViewModel
    
    var body: some View {
        HStack(alignment: .center) {
            VStack(spacing: 16) {
                TextView(title: viewModel.selectedRangeWeather?.weather.first?.main ?? "")
                
                AsyncImage(url: URL(string: "https://openweathermap.org/img/w/\(viewModel.selectedRangeWeather?.weather.first?.icon ?? "01d").png"))
                    .frame(width: 40, height: 40)
                
                TextView(title: String(viewModel.selectedRangeWeather?.main.temp ?? 0.0) + " °C")
                
            }
            
            Picker(selection: $viewModel.selectedTimeIndex, label: Text("")) {
                ForEach(Array(viewModel.weatherByTime.enumerated()), id: \.offset) { index, element in
                    Text(viewModel.weatherByTime[index].dtTxt)
                        .font(.system(size: 12, weight: .heavy))
                        .foregroundColor(Color.white)
                        .tag(index)
                }
            }
            .pickerStyle(.wheel)
            .frame(width: 200)
        }
    }
}
