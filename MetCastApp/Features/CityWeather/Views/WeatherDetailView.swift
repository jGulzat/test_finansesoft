//
//  WeatherDetailView.swift
//  MetCastApp
//
//  Created by Gulzat on 20/6/24.
//

import SwiftUI

struct WeatherDetailView: View {
    
    let weatherDetail: CityWeather
    
    var body: some View {
        HStack(spacing: 10) {
            TextView(title: "Wind: \(weatherDetail.wind.speed) m/s")
            TextView(title: "Humidity: \(weatherDetail.main.humidity) %")
            TextView(title: "Feels like: \(weatherDetail.main.feelsLike)°C")
        }
    }
}
