//
//  CityWeatherDaysView.swift
//  MetCastApp
//
//  Created by Gulzat on 20/6/24.
//

import SwiftUI

struct CityForecastBottomView: View {
    
    @ObservedObject var viewModel: CityForecastViewModel
    
    var body: some View {
        ScrollView(.horizontal, showsIndicators: false) {
            HStack {
                ForEach(Array(viewModel.cityForecast.enumerated()), id: \.offset) { index, element in
                    Button(action: {
                        viewModel.selectedDayIndex = index
                    }, label: {
                        Text(element.first?.dtTxt.eraseDate ?? "")
                            .foregroundColor(Color.white.opacity(0.9))
                            .font(.system(size: 14, weight: .regular))
                            .frame(width: 100, height: 12, alignment: .center)
                            .padding(.all, 8)
                            .cornerRadius(20)
                            .overlay(
                                RoundedRectangle(cornerRadius: 20)
                                    .stroke(
                                        viewModel.selectedDayIndex == index ? Color.blue : Color.white.opacity(0.8),
                                        lineWidth: viewModel.selectedDayIndex == index ? 4 : 1
                                    )
                            )
                    })
                }
            }
        }
    }
}
