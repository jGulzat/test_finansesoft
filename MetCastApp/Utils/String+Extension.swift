//
//  String+Extension.swift
//  MetCastApp
//
//  Created by Gulzat on 19/6/24.
//

import Foundation

extension String {
    
    var eraseDate: String {
        return self.components(separatedBy: " ").first ?? self
    }
}
